package com.example.fragment;

import android.os.Bundle;
import android.app.Fragment; //заменяем android.support.v4.app.Fragment для совместимости с android.app.Activity
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestFragment extends Fragment {

    public TestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test, container, false);

        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                * получаем ссылку на главную активность
                * и меняем текст на главной активности
                 */
                MainActivity a = (MainActivity)getActivity();
                ((EditText)a.findViewById(R.id.editTextMain)).setText("qwerty");
            }
        });

        return view;
    }

}
