package com.example.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonMain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                * получаем ссылку на фрагмент
                * и меняем текст на фрагменте
                 */
                TestFragment f = (TestFragment)getFragmentManager().findFragmentById(R.id.fragment);
                ((EditText)f.getView().findViewById(R.id.editText)).setText("asdfgh");
            }
        });
    }
}
